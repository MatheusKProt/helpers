# .sortAsc()

{% hint style="danger" %}
This method is implemented in Array.prototype
{% endhint %}

## Presentation

Sort array ascending.

## Usage

### Params

#### callback

An optional argument that can be used to specify the value to sorted.&#x20;

The callback receives from params the current interaction value.

```typescript
sortAsc(callback?: (params: T) => any): Array<T>
```

{% hint style="info" %}
That can be used to cast the params for the sort.
{% endhint %}

### Example

```javascript
[3, 1, 2].sortAsc() // [1, 2, 3]
```

```javascript
[{ order: 3 }, { order: 1 }, { order: 2 }].sortAsc(a => a.order)
// [{ order: 1 }, { order: 2 }, { order: 3 }]
```
