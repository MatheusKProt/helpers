# Introduction

## Installation

{% tabs %}
{% tab title="yarn" %}
```bash
yarn add @bemobile-tech/helpers
```
{% endtab %}

{% tab title="npm" %}
```bash
npm install @bemobile-tech/helpers
```
{% endtab %}
{% endtabs %}

## Usage

In your main file add:

```typescript
import '@bemobile-tech/helpers'
```

## Helpers

{% tabs %}
{% tab title="Array" %}
| Name                                    | Description                            |
| --------------------------------------- | -------------------------------------- |
| [.filterFalsy()](array/.filterfalsy.md) | Remove every falsy element from array. |
| [.sortAsc()](array/.sortasc.md)         | Sort array ascending.                  |
| [.sortDesc()](array/.sortdesc.md)       | Sort array descending.                 |
{% endtab %}
{% endtabs %}
