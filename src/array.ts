/* eslint-disable @typescript-eslint/no-explicit-any */
export {}

type FilterFalsy<T> = Exclude<T, false | null | undefined | '' | 0>

declare global {
  interface Array<T> {
    filterFalsy(): Array<FilterFalsy<T>>
    sortAsc(callback?: (params: T) => any): Array<T>
    sortDesc(callback?: (params: T) => any): Array<T>
  }
}

globalThis.Array.prototype.filterFalsy = function () {
  return this.filter(Boolean)
}

globalThis.Array.prototype.sortAsc = function (callback) {
  if (callback) {
    return this.sort((a: any, b: any) => (callback(a) > callback(b) ? 1 : -1))
  }

  return this.sort((a: any, b: any) => (a > b ? 1 : -1))
}

globalThis.Array.prototype.sortDesc = function (callback) {
  if (callback) {
    return this.sort((a: any, b: any) => (callback(a) < callback(b) ? 1 : -1))
  }

  return this.sort((a: any, b: any) => (a < b ? 1 : -1))
}
