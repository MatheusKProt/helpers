/**
 * This is a sample test suite.
 * Replace this with your implementation.
 */

import './index'

describe('Array', function () {
  it('should inject sortAsc to Array.prototype', function () {
    expect(Array.prototype.sortAsc).toBeDefined()
  })

  it('should inject sortDesc to Array.prototype', function () {
    expect(Array.prototype.sortDesc).toBeDefined()
  })

  it('should inject filterFalsy to Array.prototype', function () {
    expect(Array.prototype.filterFalsy).toBeDefined()
  })

  it('should order desc an array of object', function () {
    const sut = [{ order: 1 }, { order: 3 }, { order: 2 }].sortDesc(a => a.order)

    expect(sut).toStrictEqual([{ order: 3 }, { order: 2 }, { order: 1 }])
  })

  it('should order desc an array', function () {
    const sut = [1, 3, 2].sortDesc()

    expect(sut).toStrictEqual([3, 2, 1])
  })

  it('should order asc an array of object', function () {
    const sut = [{ order: 3 }, { order: 1 }, { order: 2 }].sortAsc(a => a.order)

    expect(sut).toStrictEqual([{ order: 1 }, { order: 2 }, { order: 3 }])
  })

  it('should order asc an array', function () {
    const sut = [3, 1, 2].sortAsc()

    expect(sut).toStrictEqual([1, 2, 3])
  })

  it('should filter null from array', function () {
    const sut = [3, 2, 1, null].filterFalsy()

    expect(sut).toStrictEqual([3, 2, 1])
  })

  it('should filter undefined from array', function () {
    const sut = [3, 2, 1, undefined].filterFalsy()

    expect(sut).toStrictEqual([3, 2, 1])
  })

  it('should filter 0 from array', function () {
    const sut = [3, 2, 1, 0].filterFalsy()

    expect(sut).toStrictEqual([3, 2, 1])
  })

  it('should filter "" from array', function () {
    const sut = [3, 2, 1, ''].filterFalsy()

    expect(sut).toStrictEqual([3, 2, 1])
  })

  it('should filter false from array', function () {
    const sut = [3, 2, 1, false].filterFalsy()

    expect(sut).toStrictEqual([3, 2, 1])
  })
})
