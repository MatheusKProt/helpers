# Table of contents

* [@bemobile-tech/helpers](README.md)

## Information

* [Introduction](introduction.md)

## Array

* [.filterFalsy()](array/.filterfalsy.md)
* [.sortAsc()](array/.sortasc.md)
* [.sortDesc()](array/.sortdesc.md)
